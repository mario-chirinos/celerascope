//////////////////////////////////////////////////////////////////////////////
//	CS_Functions.cpp
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	9 Mar 2008 - 17 Feb 09 - 23 May 2011
//----------------------------------------------------------------------------
//	funciones de Celerascope
//	Notas:	
//		
//////////////////////////////////////////////////////////////////////////////
#include "CS_Functions.h"
#include "ADT_Filters.h"
#include "ADTlib.h"
#include <iostream>
#include <cmath>
using namespace std;

//---------------------------------------------------------------------------
long double sLength(vector <ADT_Point2D_ui> &points)
{
	if(points.size()!=2)
		return 0;	
	
	ADT_Matrix p(2,1,0.0);//LaVectorDouble p(2);	
	p(0,0)=(double)points[0].x-(double)points[1].x;
	p(1,0)=(double)points[0].y-(double)points[1].y;
 return adtLA_norm2(p);	
}

//---------------------------------------------------------------------------
long double aLength(vector <ADT_Point2D_ui> &points)
{
	if(points.size()<2)
		return 0;	
	long double sum = 0;
	for(unsigned int i=1; i<points.size(); i++)	
	{
		ADT_Matrix p(2,1,0.0);//LaVectorDouble p(2);	
		p(0,0)=(double)points[i-1].x-(double)points[i].x;
		p(1,0)=(double)points[i-1].y-(double)points[i].y;
		sum += adtLA_norm2(p);
	}
 return sum;
}

//---------------------------------------------------------------------------
long double angle(vector <ADT_Point2D_ui> &points)
{
	if(points.size()<3)
		return 0;
	ADT_Matrix a(2,1,0.0);//LaVectorDouble a(2);	
	a(0,0)=(double)points[1].x-(double)points[0].x;
	a(1,0)=(double)points[1].y-(double)points[0].y;
	ADT_Matrix b(2,1,0.0);//LaVectorDouble b(2);	
	b(0,0)=(double)points[2].x-(double)points[0].x;
	b(1,0)=(double)points[2].y-(double)points[0].y;

	return acos(adtLA_dot(a,b)/(adtLA_norm2(a)*adtLA_norm2(b)) ); //(a'b)/(|a|*|b|)
}

//---------------------------------------------------------------------------
long double polyArea(vector <ADT_Point2D_ui> &points)//non self intersecting polygon
{
	if(points.size()<3)
		return 0;
 long double sum = 0;
 long double a, b, c, d;
	for(unsigned int i=1; i<points.size(); i++)
	{
		a = points[i-1].x; b = points[i].x;
		c = points[i-1].y; d = points[i].y;
		sum+= a*d - b*c;

	}
		a = points[points.size()-1].x; b = points[0].x;
		c = points[points.size()-1].y; d = points[0].y;
		sum+= a*d - b*c;
	return abs(0.5 * sum);
}
//---------------------------------------------------------------------------
unsigned int countParticles(const ADT_Image &image, bool median_on, vector< vector<ADT_Point2D_ui> > &segments, unsigned int &median)
{//ADD using input segments for mark the counting region
	segments.clear();
 ADT_Image imOut(image);
 ADT_Image imTemp(image);
 int n=-1;

	rgbToGray(imOut);
	rgbToGray(imTemp);
	median2D(imTemp.data, imTemp.getWidth(), imTemp.getHeight(), imTemp.getChannels(), 7);
	imOut-=imTemp;
	imOut.writeToFile("outSub.jpg","jpg");
	BiModHisTh(imOut.data, imOut.getWidth(), imOut.getHeight(), imOut.getChannels());
	imOut.writeToFile("outHist.jpg","jpg");
	segments = ccSegmentation(imOut.data, imOut.getWidth(), imOut.getHeight(),255);
		
	int *list = new int[segments.size()];
	for(unsigned int i=0; i<segments.size(); i++)
	{
		list[i]=segments[i].size();
	}

	if(median_on)
	{
		median = quick_select(list, segments.size());
		n =0;
		cout << "median: " << median << endl;
		for(unsigned int i=0; i<segments.size(); i++)
		{
			double div = (double)list[i]/ (double)median;
			double intpart;
			double frac = modf(div, &intpart);
			n+= (frac<0.5? floor(div): ceil(div));
		}
		
	}
	else
	{
		n = segments.size();
		median = 0;
	}	
	imOut.writeToFile("outCount.jpg", "jpg");
 return n;
}
//---------------------------------------------------------------------------


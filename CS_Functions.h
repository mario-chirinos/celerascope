//////////////////////////////////////////////////////////////////////////////
//	CS_Functions.h
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	9 Mar 2008 - 17 Feb 09
//----------------------------------------------------------------------------
//	funciones de Celerascope
//	Notas:	
//		
//////////////////////////////////////////////////////////////////////////////

#ifndef CS_FUNCTIONS_H
#define CS_FUNCTIONS_H

// your public header include
#include <vector>
#include "ADT_DataTypes.h"
#include "ADT_Image.h"
#include "ADT_DataTypes.h"
using namespace std;
//----------------------------------------------------------------------------
long double sLength(vector <ADT_Point2D_ui> &points);
long double aLength(vector <ADT_Point2D_ui> &points);
long double angle(vector <ADT_Point2D_ui> &points);
long double polyArea(vector <ADT_Point2D_ui> &points);
//long double aArea(vector <ADT_Point2D_ui> &points, ADT_Image image);
unsigned int countParticles(const ADT_Image &image, bool median_on, vector< vector<ADT_Point2D_ui> > &segments, unsigned int &median);
//----------------------------------------------------------------------------
#endif // CS_FUNCTIONS_H
// EOF

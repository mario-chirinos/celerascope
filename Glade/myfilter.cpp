////////////////////////////////////////////////////////////////////////////////
// filter.cpp
// Mario Chirinos Colunga
// June 21 2010
//------------------------------------------------------------------------------
// Notes:
//	c++ unit tamplate
//
////////////////////////////////////////////////////////////////////////////////
#include "myfilter.h"
#include "CS_guiClass.h"
#include "CS_callbacks.h"
#include "ADT_Jasper.h" 
//------------------------------------------------------------------------------
void myFilter(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, void* userdata)
{
	if(GTKapp->capture)
	{
		unsigned char* temp = new unsigned char[width*height*nChannels];
		memcpy (temp, data, width*height*nChannels);
		GTKapp->pixbuf = gdk_pixbuf_new_from_data(temp,  GDK_COLORSPACE_RGB, 0, 8, width, height, width*nChannels, NULL, NULL);
		
		GTKapp->capture = false;
		GTKapp->loadPixBuf = true;
		celerascope.image=ADT_Image(temp, width, height, nChannels);

		time_t rawtime;
		time ( &rawtime );
		string fileName = homedir + string("/") +  string(GTKapp->getCurrentTimeString()) + string(".jpg");	
		celerascope.image.writeToFile(fileName.c_str(),"jpg");
	}
}
//------------------------------------------------------------------------------

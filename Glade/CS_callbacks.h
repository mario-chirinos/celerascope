////////////////////////////////////////////////////////////////////////////////
//	CS_callbacks.h
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	5 nov 2008 - 30 May 2010
//------------------------------------------------------------------------------
//	Funciones de la interfaz de usuario de CeleraScope
//	Notas:	
//		
////////////////////////////////////////////////////////////////////////////////

#ifndef CS_CALLBACKS_H
#define CS_CALLBACKS_H

// your public header include
//#include <gtk/gtk.h>
#include <ADTlib.h>
#include <ADT_GTK.h>
// the declaration of your class...
int on_idle_callback(void *data);
extern "C" void on_main_window_destroy	(GtkWidget *object, void* user_data);
extern "C" void on_quit_action_activate	(GtkWidget *object, void* user_data);
extern "C" void test_callbak		(GtkWidget *object, void* user_data);
extern "C" void on_autocap_change	(GtkWidget *object, void* user_data);
extern "C" void hide_on_window_delete_event(GtkWidget *object, void* user_data); //oculatar la ventana en vez de destruirla
extern "C" void on_showAbout		(GtkWidget *object, void* user_data);
extern "C" void on_show_config		(GtkWidget *object, void* user_data);
extern "C" void on_showImg		(GtkWidget *object, void* user_data);
extern "C" void on_showVideoDev		(GtkWidget *object, void* user_data);
extern "C" void on_openImg		(GtkWidget *object, void* user_data);
extern "C" void on_microscope_combobox_changed(GtkWidget *object, void* user_data);
//extern "C" void on_microscope_comboboxcfg_changed(GtkWidget *object, void* user_data);
extern "C" void on_lens_combobox_changed(GtkWidget *object, void* user_data);
extern "C" void on_add_microscope	(GtkWidget *object, void* user_data);
extern "C" void on_remove_microscope	(GtkWidget *object, void* user_data);
extern "C" void on_add_lens		(GtkWidget *object, void* user_data);
extern "C" void on_remove_lens		(GtkWidget *object, void* user_data);
extern "C" void on_cancel_cfg		(GtkWidget *object, void* user_data);
extern "C" void on_apply_cfg		(GtkWidget *object, void* user_data);
extern "C" void on_apply_calibration	(GtkWidget *object, void* user_data);
extern "C" void on_reference_change	(GtkWidget *object, void* user_data);
extern "C" void on_PPV_button_clicked	(GtkWidget *object, void* user_data);
extern "C" void on_viewCounting_activate(GtkWidget *object, void* user_data);
extern "C" void on_ISO4406_checkbutton_toggled(GtkWidget *object, void* user_data);
extern "C" void on_equArea_checkbutton_toggled(GtkWidget *object, void* user_data);

/*extern "C" void on_canvas_mouse_event	(GnomeCanvasItem *item, GdkEvent *event, void* data);*/
/*extern "C" void on_canvas_mouse_move	(GnomeCanvasItem *item, GdkEvent *event, void* data);*/
extern "C" void on_clear_activate	(GtkWidget *object, void* user_data);

extern "C" void on_device_combobox_changed(GtkWidget *object, void* user_data);
extern "C" void on_refreshdevices_action_activate(GtkWidget *object, void* user_data);
extern "C" void on_connect_action_activate(GtkWidget *object, void* user_data);

extern "C" void on_capture_action_activate(GtkWidget *object, void* user_data);
extern "C" void on_saveImg_activate(GtkWidget *object, void* user_data);
extern "C" int on_timer(void* data);
#endif


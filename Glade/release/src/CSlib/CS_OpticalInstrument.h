//////////////////////////////////////////////////////////////////////////////
//	ADT_OpticalInstrument.h
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	8 Mar 2008 - 16 Jun 2009
//----------------------------------------------------------------------------
//	Clase complementartia de Celerascope
//	Notas:	CS_lens Zoom, char* or int?
//		
//////////////////////////////////////////////////////////////////////////////

#ifndef ADT_OPTICALINSTRUMENT_H
#define ADT_OPTICALINSTRUMENT_H

// your public header include
#include <vector>
#include <string>
// the declaration of your class...

using namespace std;
//----------------------------------------------------------------------------
class CS_Lens
{
 public:
	unsigned int 	getZoom();
	long double 	getK();
	void 		setK(long double k_);

	CS_Lens();
	CS_Lens(int zoom_, long double k_);
	~CS_Lens();		
	
 private:
	unsigned int 	zoom; //name or int?
	long double 	k; // meters/pix //kx, ky???
};
//----------------------------------------------------------------------------
class CS_OpticalInstrument
{
 public:
	const char* 	getName() const;
	int		getIndex() const;
	int 		setIndex(unsigned int i);
	unsigned int 	getSize() const;	
	unsigned int 	addItem(CS_Lens item);
	CS_Lens		getItem(unsigned int n) const;
	CS_Lens 	removeItem(unsigned int n);
	
	CS_OpticalInstrument();
	CS_OpticalInstrument(const char* name_);
	~CS_OpticalInstrument();
		
 private:
	string 		name;
	int 		index;
	vector <CS_Lens> lensesList;	
};
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#endif // ADT_OPTICALINSTRUMENT_H
// EOF

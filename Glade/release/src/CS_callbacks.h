////////////////////////////////////////////////////////////////////////////////
//	CS_callbacks.h
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	5 nov 2008 - 30 May 2010
//------------------------------------------------------------------------------
//	Funciones de la interfaz de usuario de CeleraScope
//	Notas:	
//		
////////////////////////////////////////////////////////////////////////////////

#ifndef CS_CALLBACKS_H
#define CS_CALLBACKS_H

// your public header include
//#include <gtk/gtk.h>
#include<ADT_GTK.h>
// the declaration of your class...
int on_idle_callback(void *data);
extern "C" void on_main_window_destroy	(GtkObject *object, void* user_data);
extern "C" void on_quit_action_activate	(GtkObject *object, void* user_data);
extern "C" void test_callbak		(GtkObject *object, void* user_data);
extern "C" void on_autocap_change	(GtkObject *object, void* user_data);
extern "C" void hide_on_window_delete_event(GtkObject *object, void* user_data); //oculatar la ventana en vez de destruirla
extern "C" void on_showAbout		(GtkObject *object, void* user_data);
extern "C" void on_show_config		(GtkObject *object, void* user_data);
extern "C" void on_showImg		(GtkObject *object, void* user_data);
extern "C" void on_showVideoDev		(GtkObject *object, void* user_data);
extern "C" void on_openImg		(GtkObject *object, void* user_data);
extern "C" void on_microscope_combobox_changed(GtkObject *object, void* user_data);
//extern "C" void on_microscope_comboboxcfg_changed(GtkObject *object, void* user_data);
extern "C" void on_lens_combobox_changed(GtkObject *object, void* user_data);
extern "C" void on_add_microscope	(GtkObject *object, void* user_data);
extern "C" void on_remove_microscope	(GtkObject *object, void* user_data);
extern "C" void on_add_lens		(GtkObject *object, void* user_data);
extern "C" void on_remove_lens		(GtkObject *object, void* user_data);
extern "C" void on_cancel_cfg		(GtkObject *object, void* user_data);
extern "C" void on_apply_cfg		(GtkObject *object, void* user_data);
extern "C" void on_apply_calibration	(GtkObject *object, void* user_data);
extern "C" void on_reference_change	(GtkObject *object, void* user_data);
extern "C" void on_PPV_button_clicked	(GtkObject *object, void* user_data);
extern "C" void on_viewCounting_activate(GtkObject *object, void* user_data);

extern "C" void on_canvas_mouse_event	(GnomeCanvasItem *item, GdkEvent *event, void* data);
extern "C" void on_canvas_mouse_move	(GnomeCanvasItem *item, GdkEvent *event, void* data);
extern "C" void on_clear_activate	(GtkObject *object, void* user_data);

extern "C" void on_device_combobox_changed(GtkObject *object, void* user_data);
extern "C" void on_refreshdevices_action_activate(GtkObject *object, void* user_data);
extern "C" void on_connect_action_activate(GtkObject *object, void* user_data);

extern "C" void on_capture_action_activate(GtkObject *object, void* user_data);
extern "C" void on_saveImg_activate(GtkObject *object, void* user_data);
extern "C" int on_timer(void* data);
#endif


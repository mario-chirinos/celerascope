//////////////////////////////////////////////////////////////////////////////
//	CS_guiFunctions.h
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	6 nov 2008 - 
//----------------------------------------------------------------------------
//	Funciones de la interfaz de usuario de CeleraScope
//	Notas:	
//		
//////////////////////////////////////////////////////////////////////////////
#ifndef CS_GUIFUNCTIONS_H
#define CS_GUIFUNCTIONS_H

// your public header include
#include <gtk/gtk.h>

// the declaration of your class...
//------------------------------------------------------------------------------
int guiInit();
//------------------------------------------------------------------------------

#endif

//////////////////////////////////////////////////////////////////////////////
//	CS_callbacks.cpp
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	5 nov 2008 - 30 May 2010
//----------------------------------------------------------------------------
//	Funciones de la interfaz de usuario de CeleraScope
//	Notas:	
//		
//////////////////////////////////////////////////////////////////////////////
#include "CS_callbacks.h"
#include "CS_guiClass.h"
#include <iostream>
#include <cstring>
#include <sstream>
#include <cmath>	
#include <time.h>
#include "myfilter.h"
#include <gdk/gdk.h>

using namespace std;

//------------------------------------------------------------------------------
void on_main_window_destroy(GtkObject *object, void* user_data)
{	cout << " on_main_window_destroy" << endl;
	celerascope.saveCfg("CS.cfg");
        gtk_main_quit();
}
//------------------------------------------------------------------------------
void on_quit_action_activate(GtkObject *object, void* user_data)
{
	celerascope.saveCfg("CS.cfg");
        gtk_main_quit();
}
//------------------------------------------------------------------------------
int on_idle_callback(void *data)
{
	if(GTKapp->loadPixBuf)
	{
		GTKapp->openImage(GTKapp->pixbuf);
		GTKapp->loadPixBuf = false;
	}
 return 1;
}
//------------------------------------------------------------------------------
void on_autocap_change(GtkObject *object, void* user_data)
{
static unsigned int timerID = 0;
//static int timerCounter = 0;
	if(gtk_toggle_button_get_active((GtkToggleButton*)GTKapp->autocap_checkbutton))
	{cout << "activado" <<endl;
		gtk_widget_set_sensitive (GTKapp->autocapture_vbox, true);
		if(gtk_toggle_button_get_active((GtkToggleButton*)GTKapp->limit_checkbutton))
			gtk_widget_set_sensitive (GTKapp->limit_hbox, true);
		else
		{
			gtk_widget_set_sensitive (GTKapp->limit_hbox, false);
		}
		if(timerID != 0)
			g_source_remove(timerID);
		timerID = g_timeout_add(1000*gtk_range_get_value(GTK_RANGE(GTKapp->timeScale)), on_timer, NULL);
			
	}		
	else
	{cout <<"desactivado" << endl;
		gtk_widget_set_sensitive (GTKapp->autocapture_vbox, false);
		if(timerID != 0)
			g_source_remove(timerID);
		GTKapp->timerCounter = 0;
	}
	cout << "changed" << endl;	
	cout <<   gtk_range_get_value(GTK_RANGE(GTKapp->timeScale))<< "-" << GTKapp->timerCounter << endl;

}
//------------------------------------------------------------------------------
void hide_on_window_delete_event(GtkObject *object, void* user_data)
{cout<<"hide"<<endl;
	gtk_widget_hide_on_delete((GtkWidget*)object);
}
//------------------------------------------------------------------------------
void on_show_config(GtkObject *object, void* user_data)
{
	gtk_widget_show (GTKapp->configWindow);
}
//------------------------------------------------------------------------------
void on_showAbout(GtkObject *object, void* user_data)
{
	gtk_widget_show (GTKapp->aboutdialog);
}
//------------------------------------------------------------------------------
void on_openImg(GtkObject *object, void* user_data)
{
	vector<char*> filters;
	filters.push_back((char*)"*.jpg");
	filters.push_back((char*)"*.JPG");
	const char *filename = GTKapp->fileChooserDialog(filters);
	if(filename != NULL)
		GTKapp->openImage(filename);	
}
//------------------------------------------------------------------------------
void on_showImg(GtkObject *object, void* user_data)
{
	 gtk_widget_show_all(GTKapp->imageWindow);
}
//------------------------------------------------------------------------------
void on_clear_activate(GtkObject *object, void* user_data)
{
	clearGnomeCanvas(GTKapp->canvas);
	GTKapp->pixbuf = gdk_pixbuf_new_from_data(celerascope.image.data, GDK_COLORSPACE_RGB, 0, 8, celerascope.image.getWidth(), celerascope.image.getHeight(), celerascope.image.getWidth()*celerascope.image.getChannels(), NULL, NULL);
	GTKapp->loadPixBuf = true;
}
//------------------------------------------------------------------------------
void on_microscope_combobox_changed(GtkObject *object, void* user_data)
{cout <<"on_microscope_combobox_changed"<<endl;

const char *name = gtk_buildable_get_name((GtkBuildable*)object);
cout << "name:" << name <<endl;

	if(strcmp("microscope_combobox", name)==0)
	{
		celerascope.setIndexI(gtk_combo_box_get_active((GtkComboBox*)object));
		GTKapp->refreshComboBoxes((GtkComboBox*)object, celerascope.getIndexI(), (GtkComboBox*)GTKapp->lsComboBox, celerascope.getList());
	}
	if(strcmp("microscope_combobox_cfg", name)==0 && gtk_combo_box_get_active((GtkComboBox*)object) >=0)	
	{
		if(gtk_combo_box_get_active((GtkComboBox*)object) >=0)
			GTKapp->refreshComboBoxes((GtkComboBox*)object, gtk_combo_box_get_active((GtkComboBox*)object), (GtkComboBox*)GTKapp->lsComboBoxCfg, csTemp.getList());
	}
}
//------------------------------------------------------------------------------
//void on_microscope_comboboxcfg_changed(GtkObject *object, void* user_data)
//{cout <<"on_microscope_comboboxcfg_changed"<<endl;
//cout << "index" << gtk_combo_box_get_active((GtkComboBox*)object) << endl;

//	if(gtk_combo_box_get_active((GtkComboBox*)object) >=0)
//		GTKapp->refreshComboBoxes((GtkComboBox*)object, gtk_combo_box_get_active((GtkComboBox*)object), (GtkComboBox*)GTKapp->lsComboBoxCfg, csTemp.getList());
//}
//------------------------------------------------------------------------------
void on_lens_combobox_changed(GtkObject *object, void* user_data)
{
	celerascope.setIndexL(gtk_combo_box_get_active((GtkComboBox*)object));
	stringstream ss;
	string str;
	if(celerascope.CONFIG&NOTATION)				
		ss << "1px= " << engNotation(celerascope.getK(), "m",1) << endl;
	else
		ss << "1px= " << sciNotation(celerascope.getK(), "m") << endl;

	getline(ss, str);
	gtk_label_set_label((GtkLabel *)GTKapp->areaLabel, sciNotation(celerascope.getArea(), "m²"));	
	gtk_label_set_label((GtkLabel *)GTKapp->errorLabel, str.c_str());
	cout << "on_lens_combobox_changed" << endl;
}
//------------------------------------------------------------------------------
void on_add_microscope(GtkObject *object, void* user_data)
{cout<<"on_add_microscope"<<endl;
	const char* text = gtk_entry_get_text((GtkEntry*)GTK_BIN(GTKapp->msComboBoxCfg)->child);
	CS_OpticalInstrument temp(text);
	csTemp.addItem(temp);
		GTKapp->refreshComboBoxes((GtkComboBox*)GTKapp->msComboBoxCfg, csTemp.getIndexI(), (GtkComboBox*)GTKapp->lsComboBoxCfg, csTemp.getList());
}
//------------------------------------------------------------------------------
void on_remove_microscope(GtkObject *object, void* user_data)
{cout<<"on_remove_microscope(GtkObject *object, void* user_data)"<<endl;
	csTemp.removeItem(gtk_combo_box_get_active((GtkComboBox*)GTKapp->msComboBoxCfg));
	GTKapp->refreshComboBoxes((GtkComboBox*)GTKapp->msComboBoxCfg, csTemp.getIndexI(), (GtkComboBox*)GTKapp->lsComboBoxCfg, csTemp.getList());
}
//------------------------------------------------------------------------------
void on_add_lens(GtkObject *object, void* user_data)
{cout<<"on_add_lense"<<endl;
	const char* text = gtk_entry_get_text((GtkEntry*)GTK_BIN(GTKapp->lsComboBoxCfg)->child);
	int activeItem =  gtk_combo_box_get_active((GtkComboBox*)GTKapp->msComboBoxCfg);
	stringstream ss;
	int zoom;
	ss << text;	
	ss >> zoom;
	CS_Lens tempL(zoom, 1.0);
	CS_OpticalInstrument tempI = csTemp.removeItem(activeItem);
	tempI.addItem(tempL);
	csTemp.addItem(tempI);
		GTKapp->refreshComboBoxes((GtkComboBox*)GTKapp->msComboBoxCfg, activeItem, (GtkComboBox*)GTKapp->lsComboBoxCfg, csTemp.getList());
}
//------------------------------------------------------------------------------
void on_remove_lens(GtkObject *object, void* user_data)
{
	CS_OpticalInstrument item = csTemp.removeItem(gtk_combo_box_get_active((GtkComboBox*)GTKapp->msComboBoxCfg));
	CS_Lens lens = item.removeItem(gtk_combo_box_get_active((GtkComboBox*)GTKapp->lsComboBoxCfg));
	csTemp.addItem(item);
		GTKapp->refreshComboBoxes((GtkComboBox*)GTKapp->msComboBoxCfg, csTemp.getIndexI(), (GtkComboBox*)GTKapp->lsComboBoxCfg, csTemp.getList());
}
//------------------------------------------------------------------------------
void on_cancel_cfg(GtkObject *object, void* user_data)
{
	GTKapp->guiRefresh();
	gtk_widget_hide_on_delete(GTKapp->configWindow);
}
//------------------------------------------------------------------------------
void on_apply_cfg(GtkObject *object, void* user_data)
{
	unsigned int CONFIG =0;

	if(!gtk_toggle_button_get_active ((GtkToggleButton *)GTKapp->unitsRadiobutton))
		CONFIG = CONFIG | UNITS;
	if(!gtk_toggle_button_get_active ((GtkToggleButton *)GTKapp->angleRadiobutton))
		CONFIG = CONFIG | ANGLE;
	if(!gtk_toggle_button_get_active ((GtkToggleButton *)GTKapp->notationRadiobutton))
		CONFIG = CONFIG | NOTATION;
	if(gtk_toggle_button_get_active ((GtkToggleButton *)GTKapp->medianCheckbutton))
		CONFIG = CONFIG | COUNT;
	if(!gtk_toggle_button_get_active ((GtkToggleButton *)GTKapp->volumeRadiobutton))
		CONFIG = CONFIG | VOLUME;
	if(!gtk_toggle_button_get_active ((GtkToggleButton *)GTKapp->areaRadiobutton))
		CONFIG = CONFIG | AAREA;

	csTemp.CONFIG=CONFIG;
	csTemp.setDeep(0.000001*(long double)gtk_spin_button_get_value((GtkSpinButton*)GTKapp->deep_spinbutton));
	csTemp.saveCfg("CS.cfg");
	celerascope.openCfg("CS.cfg");

	GTKapp->guiRefresh();
	gtk_widget_hide_on_delete(GTKapp->configWindow);

}
//------------------------------------------------------------------------------
void on_apply_calibration(GtkObject *object, void* user_data)
{
	stringstream ss;
	double reference;
	ss << (char*)gtk_entry_get_text((GtkEntry*)GTKapp->referenceEntry);
	ss >> reference;

	CS_OpticalInstrument item = csTemp.removeItem(gtk_combo_box_get_active((GtkComboBox*)GTKapp->msComboBoxCfg));
	CS_Lens lens = item.removeItem(gtk_combo_box_get_active((GtkComboBox*)GTKapp->lsComboBoxCfg));
	lens.setK(reference*0.000001/(double)celerascope.getTemp()); //meters/pix 
	item.addItem(lens);
	csTemp.addItem(item);

	GTKapp->refreshComboBoxes((GtkComboBox*)GTKapp->msComboBoxCfg, csTemp.getIndexI(), (GtkComboBox*)GTKapp->lsComboBoxCfg, csTemp.getList());

}
//------------------------------------------------------------------------------
void on_reference_change(GtkObject *object, void* user_data)
{cout << "on_reference_change" << endl;
	string text = (string) gtk_entry_get_text((GtkEntry*)object);
	cout << text << endl;
	if((text[text.size()-1]< '0' || text[text.size()-1] > '9'))
	{
		bool dot = false;
		for(unsigned int i =0; i< text.size()-1; i++)
			if(text[i]=='.')
			{
				dot = true;
				cout << "dot: true" << endl;
				break;
			}
		if(text[text.size()-1]!='.' || (text[text.size()-1]=='.' && dot))
			text = text.substr(0, text.size()-1);
	}
	gtk_entry_set_text ((GtkEntry*)object, text.c_str());

}
//------------------------------------------------------------------------------
void on_PPV_button_clicked(GtkObject *object, void* user_data)
{
	vector <ADT_Point2D_ui> v;
	v.push_back(ADT_Point2D_ui(0,0));
	v.push_back(ADT_Point2D_ui(celerascope.image.getWidth()-1,celerascope.image.getHeight()-1));

	double m = (double)celerascope.measure(PPV, v);
	double p = (double)celerascope.getTemp();
	cout << "m: " << m << " p: " << p << endl;
	string text;
	stringstream ss;
	ss << p;
	ss >> text;
	gtk_label_set_text((GtkLabel*)GTKapp->particleLabel, text.c_str());
	if(celerascope.CONFIG&VOLUME)
		gtk_label_set_text((GtkLabel*)GTKapp->densityLabel, engNotation(m, "p/m³"));
	else
		gtk_label_set_text((GtkLabel*)GTKapp->densityLabel, engNotation(m, "p/L"));

	const CS_CountEvent* event = (CS_CountEvent*)celerascope.getEvent(celerascope.getIndexH());
	GnomeCanvasGroup* group = gnome_canvas_root(GNOME_CANVAS(GTKapp->canvas));	
	for (unsigned int i=0; i<event->segments.size(); i++)
	{
		double x = 0;
		double y = 0;
		for(unsigned int j=0; j<event->segments[i].size(); j++)
		{
			x += event->segments[i][j].x;
			y += event->segments[i][j].y;
		}
		x/=  event->segments[i].size();
		y/=  event->segments[i].size();

		if(event->median!=0)
		{
			cout <<"median on"<<endl;
			double div = (double)event->segments[i].size()/ (double)event->median;
			double intpart;
			double frac = modf(div, &intpart);
			int n = (frac<0.5? floor(div): ceil(div));			
			stringstream ss;
			ss << n;
			ss >> text;
		}
		else
		{
			cout <<"median off"<<endl;
			text = "-1-";
		}
		if(strcmp("0", text.c_str())!=0)
		gnome_canvas_item_new (group, gnome_canvas_text_get_type(),
					"fill-color-rgba", 0xA00000F0, "scale", 10.0,
					"x", x, "y", y,
					"text", text.c_str(),
					NULL);

	}

}
//------------------------------------------------------------------------------
void on_viewCounting_activate(GtkObject *object, void* user_data)
{
	gtk_widget_show(GTKapp->countingWindow);
}
//------------------------------------------------------------------------------
void on_capture_action_activate(GtkObject *object, void* user_data)
{	cout << "capture" << endl;
	clearGnomeCanvas(GTKapp->canvas);//CLEAR CANVAS	
	GTKapp->capture = true;
}
//------------------------------------------------------------------------------
//			Canvas callbacks
//------------------------------------------------------------------------------
void on_canvas_mouse_event(GnomeCanvasItem *item, GdkEvent *event, void* data)
{

 	double mouse_x, mouse_y;
	mouse_x = event->button.x;
 	mouse_y = event->button.y;
  	gnome_canvas_item_w2i(item->parent, &mouse_x, &mouse_y);

	if(mouse_x >0 && mouse_y>0 && mouse_x < celerascope.image.getWidth() && mouse_y < celerascope.image.getHeight() )
	{
		if(celerascope.image.getChannels()==3)
		{
			g_object_set((GObject *)GTKapp->redBar, "fraction", (double)celerascope.image((int)mouse_x, (int)mouse_y,0)/255.0, NULL);
			g_object_set((GObject *)GTKapp->greenBar, "fraction", (double)celerascope.image((int)mouse_x, (int)mouse_y,1)/255.0, NULL);
			g_object_set((GObject *)GTKapp->blueBar, "fraction", (double)celerascope.image((int)mouse_x, (int)mouse_y,2)/255.0, NULL);
		}
		else
		{
			g_object_set((GObject *)GTKapp->redBar, "fraction", (double)celerascope.image((int)mouse_x, (int)mouse_y,0)/255.0, NULL);
			g_object_set((GObject *)GTKapp->greenBar, "fraction", (double)celerascope.image((int)mouse_x, (int)mouse_y,0)/255.0, NULL);
			g_object_set((GObject *)GTKapp->blueBar, "fraction", (double)celerascope.image((int)mouse_x, (int)mouse_y,0)/255.0, NULL);

		}
	}

	bool active;
	g_object_get((GObject *)GTKapp->sLength_toolbutton,"active", (GValue*)&active, NULL);
	if(active)
	{
		GTKapp->oneClickDragDrawing(mouse_x, mouse_y, event, SLENGTH);
		return;
	}

	g_object_get((GObject *)GTKapp->aLength_toolbutton,"active", (GValue*)&active, NULL);
	if(active)
	{
		GTKapp->oneClickDragDrawing(mouse_x, mouse_y, event, ALENGTH);
		return;
	}

	g_object_get((GObject *)GTKapp->polyArea_toolbutton,"active", (GValue*)&active, NULL);
	if(active)
	{
		GTKapp->oneClickDragDrawing(mouse_x, mouse_y, event, AREA);
		return;
	}

	g_object_get((GObject *)GTKapp->angle_toolbutton,"active", (GValue*)&active, NULL);
	if(active)
	{
		GTKapp->threeClicksDragDrawing(mouse_x, mouse_y, event, ANGLE);
		return;
	}	
}
//------------------------------------------------------------------------------
void on_canvas_mouse_move(GnomeCanvasItem *item, GdkEvent *event, void* data)
{
 	double mouse_x, mouse_y;
	mouse_x = event->button.x;
 	mouse_y = event->button.y;
  	gnome_canvas_item_w2i(item->parent, &mouse_x, &mouse_y);
	if(mouse_x >0 && mouse_y>0 && mouse_x < celerascope.image.getWidth() && mouse_y < celerascope.image.getHeight() )
	{
		if(celerascope.image.getChannels()==3)
		{
			g_object_set((GObject *)GTKapp->redBar, "fraction", (double)celerascope.image((int)mouse_x, (int)mouse_y,0)/255.0, NULL);
			g_object_set((GObject *)GTKapp->greenBar, "fraction", (double)celerascope.image((int)mouse_x, (int)mouse_y,1)/255.0, NULL);
			g_object_set((GObject *)GTKapp->blueBar, "fraction", (double)celerascope.image((int)mouse_x, (int)mouse_y,2)/255.0, NULL);
		}
	}

	
}
//------------------------------------------------------------------------------
int on_timer(void* data)
{
	GTKapp->timerCounter++;		
	if(gtk_toggle_button_get_active ((GtkToggleButton *)GTKapp->limit_checkbutton))
	{
		cout <<"limeted to" <<gtk_spin_button_get_value((GtkSpinButton*)GTKapp->limit_spinbutton)<<endl;
		if(GTKapp->timerCounter > gtk_spin_button_get_value((GtkSpinButton*)GTKapp->limit_spinbutton))
			gtk_toggle_button_set_active ((GtkToggleButton *)GTKapp->autocap_checkbutton, FALSE);		
	}
	time_t rawtime;
	time ( &rawtime );
	string timedate = ctime (&rawtime);
	for(int i=0; i<timedate.size(); i++)
	{
		if(timedate[i]==' ')
			timedate[i] = '_';
	}

	on_capture_action_activate(NULL, NULL);
	cout << "timer trigered" << GTKapp->timerCounter << endl;
	cout << timedate <<endl;
return true;
}
//------------------------------------------------------------------------------
void on_device_combobox_changed(GtkObject *object, void* user_data)
{
	g_signal_handlers_block_by_func (G_OBJECT (object), (void *)on_device_combobox_changed, NULL);
		GTKapp->fillResCombobox();
	g_signal_handlers_unblock_by_func (G_OBJECT (object), (void *)on_device_combobox_changed, NULL);
}
//------------------------------------------------------------------------------
void on_refreshdevices_action_activate(GtkObject *object, void* user_data)
{
	cout<<"refresh"<<endl;
	g_signal_handlers_block_by_func (G_OBJECT (GTKapp->device_combobox), (void *)on_device_combobox_changed, NULL);

		clearComboBox((GtkComboBox*)GTKapp->device_combobox);
	 	video->enumCapDev();cout << "video->enumCapDev();" << " DONE" << endl;
		for(unsigned int n = 0; n<video->getDevListSize(); n++)
		{
			stringstream ss;
			cout << n << ": " << video->getDev(n) << " : " << video->getDevName(n) << endl;
			ss << n << ": " << video->getDev(n) << " : " << video->getDevName(n) << endl;
			string str;
			getline(ss,str);
			comboboxAppendText((GtkComboBox*)GTKapp->device_combobox, str.c_str());
		}
		gtk_combo_box_set_active((GtkComboBox*)GTKapp->device_combobox, 0);		
		GTKapp->fillResCombobox();

	g_signal_handlers_unblock_by_func (G_OBJECT (GTKapp->device_combobox), (void *)on_device_combobox_changed, NULL);
}
//------------------------------------------------------------------------------
void on_connect_action_activate(GtkObject *object, void* user_data)
{
	video->connect(gtk_combo_box_get_active((GtkComboBox*)GTKapp->device_combobox), gtk_combo_box_get_active((GtkComboBox*)GTKapp->res_combobox));
	video->setFilter(&myFilter);
	video->startFilter();
}
//------------------------------------------------------------------------------
void on_saveImg_activate(GtkObject *object, void* user_data)
{
	cout<<"on_saveImg_activate"<<endl;
	GdkWindow*  window = gtk_widget_get_window(GTKapp->canvas);
	int width;// = gdk_window_get_width(window);
	int height;// = gdk_window_get_height(window);
	gdk_drawable_get_size((GdkDrawable*) window, &width, &height);
	cout << "Width: " << width << endl << "Height: " << height << endl;
	GdkPixbuf * buffer = gdk_pixbuf_get_from_drawable (NULL, (GdkDrawable*) window, gdk_drawable_get_colormap((GdkDrawable*) window), 0, 0, 0, 0, width, height);
	int channels = gdk_pixbuf_get_n_channels(buffer);
	cout << "channels:" << channels << endl;
	unsigned char* data = gdk_pixbuf_get_pixels(buffer);
//	gdk_drawable_get_size((GdkDrawable*) window, &width, &height);
//	cout << "Width: " << width << endl << "Height: " << height << endl
	ADT_Image image = ADT_Image(data, width, height, channels);
	image.writeToFile("render.jpg", "jpg");
}
//------------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////////
//	CS_guiClass.cpp
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	5 nov 2008 - 30 May 2010
//----------------------------------------------------------------------------
//	interfaz de usuario de CeleraScope
//	Notas:	
//		
//////////////////////////////////////////////////////////////////////////////
#include<iostream>
#include <sstream>
#include <iomanip>
#include "CS_guiClass.h"
#include "CS_callbacks.h"
using namespace std;
ADT_Celerascope celerascope;
ADT_Celerascope csTemp;
T_GTKapp *GTKapp;
ADT_GstVideo *video;
//------------------------------------------------------------------------------
T_GTKapp::T_GTKapp()
{
}
//------------------------------------------------------------------------------
T_GTKapp::T_GTKapp(const char *fileName)
{
        buildFromFile(fileName);
        //add your code here
        //...distance_toolbutton
     	myTheme=  gtk_icon_theme_get_default();
	 gtk_icon_theme_append_search_path(myTheme, "icons/");  
 
        window = GTK_WIDGET (gtk_builder_get_object (builder, "main_window"));
        	//auto capture
        	autocap_checkbutton = GTK_WIDGET (gtk_builder_get_object (builder, "autocap_checkbutton"));
		autocapture_vbox = GTK_WIDGET (gtk_builder_get_object (builder, "autocapture_vbox"));
		limit_checkbutton = GTK_WIDGET (gtk_builder_get_object (builder, "limit_checkbutton"));
		limit_hbox = GTK_WIDGET (gtk_builder_get_object (builder, "limit_hbox"));
		timeScale = GTK_WIDGET (gtk_builder_get_object (builder, "time_hscale"));
		limit_spinbutton = GTK_WIDGET (gtk_builder_get_object (builder, "limit_spinbutton"));
		//data
		redBar = GTK_WIDGET (gtk_builder_get_object (builder, "red_progressbar"));
		greenBar = GTK_WIDGET (gtk_builder_get_object (builder, "green_progressbar"));
		blueBar = GTK_WIDGET (gtk_builder_get_object (builder, "blue_progressbar"));
	
		//instrument
		msComboBox = GTK_WIDGET (gtk_builder_get_object (builder, "microscope_combobox"));
		lsComboBox = GTK_WIDGET (gtk_builder_get_object (builder, "lens_combobox"));
		msComboBoxCfg = GTK_WIDGET (gtk_builder_get_object (builder, "microscope_combobox_cfg"));

		deviceMenu = GTK_WIDGET (gtk_builder_get_object (builder, "device_menu"));
		menuBar = GTK_WIDGET (gtk_builder_get_object (builder, "main_menubar"));
		
		areaLabel = GTK_WIDGET (gtk_builder_get_object (builder, "areaLabel"));
		errorLabel = GTK_WIDGET (gtk_builder_get_object (builder, "errorLabel"));


	configWindow = GTK_WIDGET (gtk_builder_get_object (builder, "config_window"));
		//Configuration window items
		lsComboBoxCfg = GTK_WIDGET (gtk_builder_get_object (builder, "lens_combobox_cfg"));
		measureLabel = GTK_WIDGET (gtk_builder_get_object (builder, "measure_label"));
		referenceEntry = GTK_WIDGET (gtk_builder_get_object (builder, "reference_entry"));

		unitsRadiobutton = GTK_WIDGET (gtk_builder_get_object (builder, "impUnits_radiobutton"));
		angleRadiobutton = GTK_WIDGET (gtk_builder_get_object (builder, "radAngle_radiobutton"));
		notationRadiobutton = GTK_WIDGET (gtk_builder_get_object (builder, "sciNotation_radiobutton"));
		medianCheckbutton = GTK_WIDGET (gtk_builder_get_object (builder, "equArea_checkbutton"));
		volumeRadiobutton = GTK_WIDGET (gtk_builder_get_object (builder, "L_radiobutton"));
		areaRadiobutton = GTK_WIDGET (gtk_builder_get_object (builder, "textureArea_radiobutton"));
		deep_spinbutton =GTK_WIDGET (gtk_builder_get_object (builder, "deep_spinbutton"));
		
		device_combobox = GTK_WIDGET (gtk_builder_get_object (builder, "device_combobox"));
		res_combobox = GTK_WIDGET (gtk_builder_get_object (builder, "res_combobox"));
		
		
	aboutdialog = GTK_WIDGET (gtk_builder_get_object (builder, "aboutdialog"));

	//counting window items
	countingWindow = GTK_WIDGET (gtk_builder_get_object (builder, "count_window"));
		particleLabel = GTK_WIDGET (gtk_builder_get_object (builder, "particle_label"));
		densityLabel = GTK_WIDGET (gtk_builder_get_object (builder, "density_label"));	

	//menu bar items
	sLength_toolbutton = GTK_WIDGET (gtk_builder_get_object (builder, "sLength_toolbutton"));
	aLength_toolbutton = GTK_WIDGET (gtk_builder_get_object (builder, "aLength_toolbutton"));
	polyArea_toolbutton = GTK_WIDGET (gtk_builder_get_object (builder, "polyArea_toolbutton"));
	angle_toolbutton = GTK_WIDGET (gtk_builder_get_object (builder, "angle_toolbutton"));

	//Build canvas Window
	imageWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  		gtk_window_set_title(GTK_WINDOW(imageWindow), "Canvas Window");
		gtk_signal_connect(GTK_OBJECT(imageWindow), "delete_event", GTK_SIGNAL_FUNC(hide_on_window_delete_event), NULL);
		scrolledWindow = gtk_scrolled_window_new(NULL, NULL);
		gtk_scrolled_window_set_policy((GtkScrolledWindow*)scrolledWindow, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		canvas = gnome_canvas_new_aa();
		gtk_container_add(GTK_CONTAINER(scrolledWindow), canvas);
		 gtk_container_add(GTK_CONTAINER(imageWindow), scrolledWindow);
		gtk_window_set_default_size((GtkWindow*)imageWindow, 640, 480);

	g_object_unref (G_OBJECT (builder)); 

	loadPixBuf = false;
	capture = false;
	pixbuf = NULL;
}
//------------------------------------------------------------------------------
//		Member functions
//------------------------------------------------------------------------------
void T_GTKapp::guiInit()
{
	//tempList = celerascope.getList();

        gtk_combo_box_entry_set_text_column( GTK_COMBO_BOX_ENTRY(msComboBoxCfg ), 0 ); 
        gtk_combo_box_entry_set_text_column( GTK_COMBO_BOX_ENTRY(lsComboBoxCfg ), 0 );

	gtk_range_set_value(GTK_RANGE(timeScale), 50);
	gtk_spin_button_set_value((GtkSpinButton*)limit_spinbutton, 10);
	guiRefresh();
}
//------------------------------------------------------------------------------
void T_GTKapp::guiRefresh()
{	
	cout << "guiRefresh()"<< endl;
	csTemp.openCfg("CS.cfg");
	refreshComboBoxes((GtkComboBox*)msComboBox, celerascope.getIndexI(), (GtkComboBox*)lsComboBox, celerascope.getList());
	refreshCfgBox();
}
//------------------------------------------------------------------------------
void T_GTKapp::refreshCfgBox()
{cout << "refreshCfgBox()"<< endl;

	csTemp.openCfg("CS.cfg");
	refreshComboBoxes((GtkComboBox*)msComboBoxCfg, celerascope.getIndexI(), (GtkComboBox*)lsComboBoxCfg, csTemp.getList());
	//refreshMeasureLabel();

	gtk_toggle_button_set_active((GtkToggleButton*)unitsRadiobutton, !((celerascope.CONFIG & UNITS)&&true));
	gtk_toggle_button_set_active((GtkToggleButton*)angleRadiobutton, !((celerascope.CONFIG & ANGLE)&&true));
	gtk_toggle_button_set_active((GtkToggleButton*)notationRadiobutton, !((celerascope.CONFIG & NOTATION)&&true));
	gtk_toggle_button_set_active((GtkToggleButton*)medianCheckbutton, ((celerascope.CONFIG & COUNT)&&true));
	gtk_toggle_button_set_active((GtkToggleButton*)volumeRadiobutton, !((celerascope.CONFIG & VOLUME)&&true));
	gtk_toggle_button_set_active((GtkToggleButton*)areaRadiobutton, !((celerascope.CONFIG & AAREA)&&true));
	gtk_spin_button_set_value((GtkSpinButton*)deep_spinbutton, 1000000*celerascope.getDeep());
}
//------------------------------------------------------------------------------
void T_GTKapp::refreshMeasureLabel()
{
	stringstream ss;
	string text;
	ss << fixed << setprecision(1) << celerascope.getTemp() << " pixels";
	getline(ss, text);
	gtk_label_set_text((GtkLabel*) measureLabel, text.c_str());
}
//------------------------------------------------------------------------------
void T_GTKapp::refreshComboBoxes(GtkComboBox* msCB, int indexI, GtkComboBox* lsCB, vector <CS_OpticalInstrument> list)
{
g_signal_handlers_block_by_func (G_OBJECT (msCB), (void *)on_microscope_combobox_changed, NULL);
g_signal_handlers_block_by_func (G_OBJECT (lsCB), (void *)on_microscope_combobox_changed, NULL);

	clearComboBox((GtkComboBox*)msCB);
	for(unsigned int i = 0; i< list.size(); i++)
		comboboxAppendText(msCB, list[i].getName());

	clearComboBox((GtkComboBox*)lsCB);
	CS_OpticalInstrument instrument = list[indexI];
	for(unsigned int i = 0; i< instrument.getSize(); i++)
	{
		int zoom = instrument.getItem(i).getZoom();
		ostringstream stm;
		stm << zoom;
		comboboxAppendText(lsCB, stm.str().c_str());
	}

	gtk_combo_box_set_active((GtkComboBox*)msCB, indexI);
	gtk_combo_box_set_active((GtkComboBox*)lsCB, instrument.getIndex());

g_signal_handlers_unblock_by_func (G_OBJECT (msCB), (void *)on_microscope_combobox_changed, NULL);
g_signal_handlers_unblock_by_func (G_OBJECT (lsCB), (void *)on_microscope_combobox_changed, NULL);
}
//------------------------------------------------------------------------------
const char* T_GTKapp::fileChooserDialog(vector<char*> filters)
{
	char *filename = NULL;
	GtkWidget *dialog;
	dialog = gtk_file_chooser_dialog_new ("Open Image",
					      (GtkWindow*)window,
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					      NULL);

	for(unsigned int i=0; i<filters.size(); i++)
	{
		GtkFileFilter *filter = gtk_file_filter_new ();
		gtk_file_filter_add_pattern (filter, filters[i]);
		gtk_file_chooser_add_filter((GtkFileChooser*)dialog, filter);
	}

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
	{
		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
	}
	gtk_widget_destroy (dialog);
	return filename;
}
//------------------------------------------------------------------------------
void T_GTKapp::openImage(GdkPixbuf *pixbufIn)
{
	GdkPixbuf *_pixbuf =  gdk_pixbuf_copy(pixbufIn);
	clearGnomeCanvas(canvas);//CLEAR CANVAS

	GnomeCanvasGroup* group	= gnome_canvas_root(GNOME_CANVAS(canvas));
	GnomeCanvasItem* item	= gnome_canvas_item_new (group, gnome_canvas_pixbuf_get_type (),
					"pixbuf", _pixbuf,
					"x", 0.0, "y", 0.0,
					"width", (double) gdk_pixbuf_get_width(_pixbuf),
					"height", (double) gdk_pixbuf_get_height(_pixbuf),
					       NULL);

	gtk_signal_connect((GtkObject*)item, "event", G_CALLBACK(on_canvas_mouse_event), NULL);
	//gtk_signal_connect((GtkObject*)item, "event", G_CALLBACK(on_canvas_mouse_move), NULL);

	gtk_window_resize(GTK_WINDOW(imageWindow), gdk_pixbuf_get_width(_pixbuf)+20, gdk_pixbuf_get_height(_pixbuf)+20);
	gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas), 0, 0, gdk_pixbuf_get_width(_pixbuf), gdk_pixbuf_get_height(_pixbuf));
	
	time_t rawtime;
	time ( &rawtime );
	string timedate = string("Captured at ") + ctime (&rawtime);
	gtk_window_set_title(GTK_WINDOW(imageWindow), timedate.c_str());
	gdk_pixbuf_unref(_pixbuf);
 	gtk_widget_show_all(imageWindow);

}
//------------------------------------------------------------------------------
void T_GTKapp::openImage(const char *fileName)
{
	pixbuf = gdk_pixbuf_new_from_file(fileName, NULL);
	openImage(pixbuf);
  	 gtk_window_set_title(GTK_WINDOW(imageWindow), fileName);
	celerascope.image=ADT_Image(fileName);
	grayToRgb(celerascope.image);	//pixbuffers just allow RGB mode
}
//------------------------------------------------------------------------------
void T_GTKapp::fillResCombobox() const
{
	int dev = gtk_combo_box_get_active((GtkComboBox*)device_combobox);
	clearComboBox((GtkComboBox*)res_combobox);

	for(unsigned int n = 0; n<video->getResListSize(dev); n++)
	{
		stringstream ss;
		ss << n << ": " << video->getRes(dev,n).x << ", " << video->getRes(dev,n).y << endl;
		string str;
		getline(ss,str);
		comboboxAppendText((GtkComboBox*)res_combobox, str.c_str());
	}
	gtk_combo_box_set_active((GtkComboBox*)res_combobox, 0);
}
//------------------------------------------------------------------------------
void T_GTKapp::refreshDeviceMenu()
{

}
//------------------------------------------------------------------------------
//			Drawing on canvas
//------------------------------------------------------------------------------
void T_GTKapp::oneClickDragDrawing(int mouse_x, int mouse_y, GdkEvent *event, unsigned int mType)
{
	static GnomeCanvasItem* newItem = NULL; 
	static GnomeCanvasItem* textItem = NULL; 
	static bool pressed = false;
	static bool moved = false;
	static double oldX, oldY;
	static vector <ADT_Point2D_ui> myPoints;
	GnomeCanvasGroup* group = gnome_canvas_root(GNOME_CANVAS(canvas));	
	GnomeCanvasPoints *points;	
	switch (event->type)
	{
		case GDK_BUTTON_PRESS:
			switch(event->button.button)
			{
				case 1:
					oldX=mouse_x; oldY=mouse_y;
					newItem = gnome_canvas_item_new (group, gnome_canvas_ellipse_get_type(),
						"x1", oldX-20, "y1", oldY-20, "x2", oldX+20, "y2", oldY+20,
						"outline_color_rgba", 0x0000FF80, "width_units", 4.0, NULL); 
					textItem = gnome_canvas_item_new (group, gnome_canvas_text_get_type(),
						"fill-color", "red", "scale", 10.0,
						"x", oldX, "y", oldY,
						"text", "test", NULL);
					myPoints.clear();
					myPoints.push_back(ADT_Point2D_ui(mouse_x ,mouse_y));
					pressed = true;
					break;
				default:
					break;
			}
			cout << "Button was presed" << endl;
	     		break;

		case GDK_MOTION_NOTIFY:
			if ((event->motion.state & GDK_BUTTON1_MASK))
			{
				//cout<< "Moving mouse with Button 1 pressed" << endl;
			}
			if(pressed)
			{
				moved = true;
				gtk_object_destroy(GTK_OBJECT(newItem));
 				gtk_object_destroy(GTK_OBJECT(textItem));
				switch (mType)
				{
					case SLENGTH:
						points = gnome_canvas_points_new(2);
						points->coords[0]=oldX;
						points->coords[1]=oldY;
						points->coords[2]=mouse_x;
						points->coords[3]=mouse_y;
						newItem = gnome_canvas_item_new (group, gnome_canvas_line_get_type(),
							"points" , points,
							"fill_color_rgba", 0x0000FF80,
							"width_units", 4.0,
							"first-arrowhead",  true,
							"last-arrowhead",  true,
							"arrow-shape-a", 5.0,
							"arrow-shape-b", 5.0,
							"arrow-shape-c", 5.0,
							"cap-style", GDK_CAP_ROUND,
							NULL); 
						myPoints.clear();
						myPoints.push_back(ADT_Point2D_ui(oldX, oldY));
						myPoints.push_back(ADT_Point2D_ui(mouse_x, mouse_y));
						break;

					case ALENGTH:
						myPoints.push_back(ADT_Point2D_ui(mouse_x,mouse_y));

						points = gnome_canvas_points_new(myPoints.size());
						for(unsigned int i=0; i<myPoints.size(); i++)
						{
							points->coords[2*i]=myPoints[i].x;
							points->coords[2*i+1]=myPoints[i].y;
						}

						newItem = gnome_canvas_item_new (group, gnome_canvas_line_get_type(),
							"points" , points,
							"fill_color_rgba", 0x0000FF80,
							"width_units", 4.0,
							"first-arrowhead",  true,
							"last-arrowhead",  true,
							"arrow-shape-a", 5.0,
							"arrow-shape-b", 5.0,
							"arrow-shape-c", 5.0,
							"cap-style", GDK_CAP_ROUND,
							NULL);

						break;
					case AREA:
						myPoints.push_back(ADT_Point2D_ui(mouse_x,mouse_y));

						points = gnome_canvas_points_new(myPoints.size()+1);
						for(unsigned int i=0; i<myPoints.size(); i++)
						{
							points->coords[2*i]=myPoints[i].x;
							points->coords[2*i+1]=myPoints[i].y;
						}
						points->coords[2*myPoints.size()]=myPoints[0].x;
						points->coords[2*myPoints.size()+1]=myPoints[1].y;
						newItem = gnome_canvas_item_new (group, gnome_canvas_polygon_get_type(),
							"points" , points,
							"fill_color_rgba", 0xFFFF00A0,
							"outline-color_rgba", 0x0000FFB0,
							"width_units", 2.0,
							NULL); 
						break;
					default:
						break;
				}
				gnome_canvas_points_unref(points);

				double xText=0; double yText=0;
				for(unsigned int i=0; i<myPoints.size(); i++)
				{
					xText+=myPoints[i].x;
					yText+=myPoints[i].y;
				}
				xText/=myPoints.size(); yText/=myPoints.size();

				const char *measure;
				if(mType==AREA)
					measure = sciNotation(celerascope.measure(mType, myPoints), "m²");
				else
					if(celerascope.CONFIG&NOTATION)				
						measure = engNotation(celerascope.measure(mType, myPoints), "m",1);
					else
						measure = sciNotation(celerascope.measure(mType, myPoints), "m");
				celerascope.undo();
				textItem = gnome_canvas_item_new (group, gnome_canvas_text_get_type(),
					"fill-color-rgba", 0xA00000F0, "scale", 10.0,
					"x", xText, "y", yText,
					"text", string(measure).c_str(),
					NULL);
				refreshMeasureLabel();
			}
			break;
         
		case GDK_BUTTON_RELEASE:
				cout<< "Button released" << endl;
				if(moved)
				{
					newItem=NULL;
					textItem=NULL;
					celerascope.redo();
				}
				else
				{
					gtk_object_destroy(GTK_OBJECT(newItem));
 					gtk_object_destroy(GTK_OBJECT(textItem));
				}
				pressed = false;
				moved = false;
			break;         
		default:
			break;
	}
	
}
//------------------------------------------------------------------------------
void T_GTKapp::threeClicksDragDrawing(int mouse_x, int mouse_y, GdkEvent *event, unsigned int mType)
{
	static GnomeCanvasItem* newItem = NULL; 
	static GnomeCanvasItem* textItem = NULL; 
//	static bool pressed = false;
	static bool moved = false;
	static int nClick = 0;
	static double oldX, oldY;
	static vector <ADT_Point2D_ui> myPoints;
	GnomeCanvasGroup* group = gnome_canvas_root(GNOME_CANVAS(canvas));	
	GnomeCanvasPoints *points;	
	switch (event->type)
	{
		case GDK_BUTTON_PRESS:
			switch(event->button.button)
			{
				case 1:
					nClick++;
					oldX=mouse_x; oldY=mouse_y;
					if(nClick==1)
					{
						newItem = gnome_canvas_item_new (group, gnome_canvas_ellipse_get_type(),
							"x1", oldX-20, "y1", oldY-20, "x2", oldX+20, "y2", oldY+20,
							"outline_color_rgba", 0xFF000080, "width_units", 4.0, NULL); 
					}
					newItem = gnome_canvas_item_new (group, gnome_canvas_ellipse_get_type(),
						"x1", oldX-20, "y1", oldY-20, "x2", oldX+20, "y2", oldY+20,
						"outline_color_rgba", 0x0000FF80, "width_units", 4.0, NULL); 
					textItem = gnome_canvas_item_new (group, gnome_canvas_text_get_type(),
						"fill-color", "red", "scale", 10.0,
						"x", oldX, "y", oldY,
						"text", "test", NULL);
//					myPoints.clear();
					myPoints.push_back(ADT_Point2D_ui(mouse_x ,mouse_y));
					
					if(nClick>2)
					{
						cout << "Button released" << endl;
						gtk_object_destroy(GTK_OBJECT(newItem));
		 				gtk_object_destroy(GTK_OBJECT(textItem));
						newItem=NULL;
						textItem=NULL;
						celerascope.redo();
						moved = false;
						nClick = 0;
						myPoints.clear();
					}
					break;
				default:
					break;
			}
	     		break;

		case GDK_MOTION_NOTIFY:
			if ((event->motion.state & GDK_BUTTON1_MASK))
			{
				//cout<< "Moving mouse with Button 1 pressed" << endl;
			}
			myPoints.push_back(ADT_Point2D_ui(mouse_x, mouse_y));
			if(nClick>0)
			{
				moved = true;
				gtk_object_destroy(GTK_OBJECT(newItem));
 				gtk_object_destroy(GTK_OBJECT(textItem));
				switch (mType)
				{
					case ANGLE:
						points = gnome_canvas_points_new(2);
						points->coords[0]=myPoints[0].x;
						points->coords[1]=myPoints[0].y;
						points->coords[2]=mouse_x;
						points->coords[3]=mouse_y;
						newItem = gnome_canvas_item_new (group, gnome_canvas_line_get_type(),
							"points" , points,
							"fill_color_rgba", 0x0000FF80,
							"width_units", 4.0,
							"first-arrowhead",  true,
							"last-arrowhead",  true,
							"arrow-shape-a", 5.0,
							"arrow-shape-b", 5.0,
							"arrow-shape-c", 5.0,
							"cap-style", GDK_CAP_ROUND,
							NULL); 
						gtk_signal_connect(GTK_OBJECT(newItem), "event", (GtkSignalFunc) on_canvas_mouse_event, NULL);
////						newItem = gnome_canvas_item_new (group, gnome_canvas_ellipse_get_type(),
////						"x1", mouse_x-20, "y1", mouse_y-20, "x2", mouse_x+20, "y2", mouse_y+20,
////						"outline_color_rgba", 0x0000FF80, "width_units", 4.0, NULL); 
						//myPoints.clear();
//						myPoints.push_back(ADT_Point2D_ui(oldX, oldY));
						break;
					default:
						break;
				}
				if(nClick>1)
				{
					gnome_canvas_points_unref(points);

					double xText=0; double yText=0;
					for(unsigned int i=0; i<myPoints.size(); i++)
					{
						xText+=myPoints[i].x;
						yText+=myPoints[i].y;
					}
					xText/=myPoints.size(); yText/=myPoints.size();

					const char *measure;
					stringstream ss;
					string str;
					ss << celerascope.measure(mType, myPoints) << (celerascope.CONFIG&ANGLE? "°": "");
					getline(ss, str);
					celerascope.undo();
					textItem = gnome_canvas_item_new (group, gnome_canvas_text_get_type(),
						"fill-color-rgba", 0xA00000F0, "scale", 10.0,
						"x", xText, "y", yText,
						"text", str.c_str(),
						NULL);
				}
			}
			myPoints.pop_back();
			break;
         
		case GDK_BUTTON_RELEASE:

			break;         
		default:
			break;
	}

}
//------------------------------------------------------------------------------

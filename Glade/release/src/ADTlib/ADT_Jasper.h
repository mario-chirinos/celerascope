//////////////////////////////////////////////////////////////////////////////
//	ADT_Jasper.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	02 Feb 2008 - 02 Jul 2008
//----------------------------------------------------------------------------
//	Jasper[1] implementation 
//	[1] 	Copyright (c) 2001-2006 Michael David Adamsx	
//		Copyright (c) 1999-2000 Image Power, Inc.
//		Copyright (c) 1999-2000 The University of British Columbia 
//		http://www.ece.uvic.ca/~mdadams/jasper/
//////////////////////////////////////////////////////////////////////////////

#ifndef ADT_JASPER_H
#define ADT_JASPER_H

// your public header include
#include <jasper/jasper.h>

bool ADT_jasper_openImage(unsigned char *&data, unsigned int &width, unsigned int &height, unsigned int &nChannels, const char *name);
bool ADT_jasper_saveImage(unsigned char *data, unsigned int width, unsigned int height, unsigned int nChannels, const char *name, const char *format);
void printImgInfo(jas_image_t *image);
#endif // ADT_JASPER_H
// EOF

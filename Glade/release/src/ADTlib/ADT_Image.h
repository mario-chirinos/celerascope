//////////////////////////////////////////////////////////////////////////////
//	ADT_Image.h
//	Mario Chirinos Colunga
//	School of Computer Science and Information Systems.
//	Birkbeck, Univerity of London.
//	25 May 2007 - 28 May 2007- Nov 2007 - Feb 2008
//----------------------------------------------------------------------------
//	Base calss for image manipulation
//	Notes:
//		
//////////////////////////////////////////////////////////////////////////////
#ifndef ADT_IMAGE_H
#define ADT_IMAGE_H

// your public header include
//..
// the declaration of your class...
class ADT_Image
{
 public:
	unsigned char	*data;
//	bool 		read(const char* fileName); //comented on 020208
	bool 		writeToFile(const char* fileName, const char* format);
	unsigned int 	getWidth() const;
	unsigned int 	getHeight() const;
	unsigned int	getChannels() const;
	bool 		initialized();

	ADT_Image();
	ADT_Image(unsigned int width_, unsigned int height_, unsigned int nChannels_);
	ADT_Image(unsigned char* data_, unsigned int width_, unsigned int height_, unsigned int nChannels_);
	ADT_Image(const char *fileName);
	ADT_Image(const ADT_Image &image);
	~ADT_Image();
	
	ADT_Image &operator-=(ADT_Image &param);
	ADT_Image operator-(ADT_Image &param) const;
	ADT_Image &operator = (const ADT_Image &param);
	int  operator() (unsigned int x, unsigned int y, unsigned int channel) const;

 private:
	unsigned int 	width, height;
	unsigned int 	nChannels;
	bool 		init;
	void		copy(const ADT_Image &i); //moved from public 26/06/09
};
//----------------------------------------------------------------------------
void imAbsDif(unsigned char *data1, unsigned char *dataIn, unsigned int width, unsigned int height, unsigned int nChannels);
#endif // ADTLIB_ADT_IMAGE_H
// EOF

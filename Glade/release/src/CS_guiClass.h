//////////////////////////////////////////////////////////////////////////////
//	CS_guiClass.h
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	5 nov 2008 - 30 May 2010
//----------------------------------------------------------------------------
//	interfaz de usuario de CeleraScope
//	Notas:	
//		
//////////////////////////////////////////////////////////////////////////////
#ifndef CS_GUICLASS_H
#define CS_GUICLASS_H
// your public header include
#include <ADT_GTK.h>
#include <ADT_Tools.h>
#include <ADT_ColorSpace.h>
#include "ADT_Celerascope.h"
#include "ADT_GStreamer.h"
// the declaration of your class...
//------------------------------------------------------------------------------
class T_GTKapp : public ADT_GTK
{
 public:
	int timerCounter;
	bool capture;
	bool loadPixBuf;
	GdkPixbuf *pixbuf;

	GtkWidget *window;
		//main window items
		GtkWidget *autocap_checkbutton;
		GtkWidget *autocapture_vbox;
		GtkWidget *limit_checkbutton;
		GtkWidget *limit_hbox;
		GtkWidget *timeScale;
		GtkWidget *limit_spinbutton;

		GtkWidget *redBar;
		GtkWidget *greenBar;
		GtkWidget *blueBar;
		GtkWidget *errorLabel;
		GtkWidget *areaLabel;

		GtkWidget *statusBar;

		GtkWidget *deviceMenu;
	
	GtkWidget *configWindow;
	GtkWidget *aboutdialog;

	GtkWidget *menuBar;

	//Configuration window items
	GtkWidget *msComboBox;
	GtkWidget *lsComboBox;
	GtkWidget *msComboBoxCfg;
	GtkWidget *lsComboBoxCfg;
	GtkWidget *measureLabel;
	GtkWidget *referenceEntry;
	
	GtkWidget *unitsRadiobutton;
	GtkWidget *unitsOFFRadiobutton;
	GtkWidget *angleRadiobutton;
	GtkWidget *notationRadiobutton;
	GtkWidget *medianCheckbutton;
	GtkWidget *volumeRadiobutton;
	GtkWidget *areaRadiobutton;

	GtkWidget *deep_spinbutton;

	GtkWidget *device_combobox;
	GtkWidget *res_combobox;

	//counting window items
	GtkWidget *countingWindow;
	GtkWidget *particleLabel;
	GtkWidget *densityLabel;
	
	//menu bar items
	GtkWidget *sLength_toolbutton;
	GtkWidget *aLength_toolbutton;
	GtkWidget *polyArea_toolbutton;
	GtkWidget *angle_toolbutton;

	// For building canvas window
	GtkWidget *imageWindow;
	GtkWidget *canvas;
	GtkWidget *scrolledWindow;
	
	GtkIconTheme *myTheme;
	
	//member functions
	T_GTKapp();
	T_GTKapp(const char *fileName);
	void guiInit();
	
	void openImage(GdkPixbuf *pixbuf);
	void openImage(const char *fileName);
	void captureImage(GdkPixbuf *pixbuf, const char* filename);
	const char* fileChooserDialog(vector<char*> filters);

	void guiRefresh();
	void refreshComboBoxes(GtkComboBox* msCB, int indexI, GtkComboBox* lsCB, vector <CS_OpticalInstrument> list);
	void refreshCfgBox();
	void refreshMeasureLabel();
	void refreshDeviceMenu();
	void oneClickDragDrawing(int mouse_x, int mouse_y, GdkEvent *event, unsigned int mType);
	void threeClicksDragDrawing(int mouse_x, int mouse_y, GdkEvent *event, unsigned int mType);
	void fillResCombobox() const;
};
//------------------------------------------------------------------------------
extern T_GTKapp *GTKapp;
extern ADT_GstVideo *video;
extern ADT_Celerascope celerascope;
extern ADT_Celerascope csTemp;
#endif

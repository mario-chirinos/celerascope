////////////////////////////////////////////////////////////////////////////////
//
//
//gcc -Wall -g main.c -o GTKapp -export-dynamic `pkg-config --cflags --libs gtk+-2.0`
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
//#include <gtk/gtk.h>
#include "CS_guiClass.h"
#include "CS_callbacks.h"
using namespace std;
//------------------------------------------------------------------------------
int main (int argc, char *argv[])
{
	gtk_init (&argc, &argv);
	gst_init (&argc, &argv);

	GTKapp =new T_GTKapp("CS_ui.glade");
	g_idle_add_full(G_PRIORITY_LOW*10, on_idle_callback, NULL, NULL);
	celerascope = ADT_Celerascope("CS.cfg");
        gtk_widget_show (GTKapp->window);
	video = new ADT_GstVideo();
	GTKapp->guiInit();
        gtk_main ();

 return 0;
}

//gcc -Wall -g -o tutorial main.c -export-dynamic `pkg-config --cflags --libs gtk+-2.0`

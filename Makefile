#Mario Chirinos Colunga
#--------------------------------------
APPNAME = celerascope
#Compiler:
	CC=g++
#Compiler flags
	CFLAGS=-c -g -Wall
#Directories
	#ifdef linux
		DIRlib= /usr/local/lib	
		libADT= ../adtlib
		incDIR= /usr/local/include
		CSdir = ../celerascope
	#endif
#main function
	mainP= CSmain
#--------------------------------------
all: Project

Project: mainP.o
	$(CC) -o $(APPNAME) \
	-L $(DIRlib) \
	-I $(incDIR) \
	-L$(CSdir) -L$(libADT) \
	$(mainP).o \
	-lCS -lADT \
	-ljasper -lblas
	

mainP.o: $(mainP).cpp
	$(CC) $(CFLAGS) \
	-I $(libADT) \
	$(mainP).cpp \
	-lADT -lCS

#g++ opencvTest1.cpp -o Prueba1 -I /usr/local/include/opencv -L /usr/local/lib -lm -lcv -lhighgui -lcvaux

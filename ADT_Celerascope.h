////////////////////////////////////////////////////////////////////////////////
//	ADT_Celerascope.h
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	7 Mar 2008 - 21 May 11
//------------------------------------------------------------------------------
//	Clase base de Celerascope
//	Notas:	
//		
////////////////////////////////////////////////////////////////////////////////

#ifndef ADT_CELERASCOPE_H
#define ADT_CELERASCOPE_H

// your public header include
#include <vector>
#include "ADT_Image.h"
#include "ADT_DataTypes.h"
#include "CS_OpticalInstrument.h"

// the declaration of your class...
#define		WEBSERVICE	"http://www.aurea-dt.com/ws/ws.php"
// Mesuarements Types
#define		SLENGTH		1
#define		ALENGTH		2
#define		AREA		3
#define		ANGLE		4
#define		AUTOAREA	5
#define		PPV		6
#define		ISO4406		7
// Configuration Flags
#define		UNITS		1	// 1=SI, 0=Imperial
#define		NOTATION	2	// 1=ENG, 0= SCI
//#define	ANGLE		4	// 1=DEG, 0=rad
#define		VOLUME		8	// 1=m3, 0=L
#define		COUNT		16	// 1=Median on, 0=Median off
#define		COUNTISO4406	32	// 	
#define		AAREA		64	// 1=Texture, 0=Color.
#define		MOSAIC		128	// 1=on, 0=off
using namespace std;
class CS_BasicEvent
{
 public:
	int 		type;
	//const char*	units;
	long double 	result;
	long double 	temp;
	vector <ADT_Point2D_ui> points;	
};
class CS_CountEvent:public CS_BasicEvent
{
 public:
	vector < vector<ADT_Point2D_ui> > segments;
	unsigned int median;
};
class CS_ISO4406Event:public CS_CountEvent
{
 public:
 	vector<long double> lengths;
	int code[3];
	int ppv[3];
};
//------------------------------------------------------------------------------
class ADT_Celerascope
{
 public:
	unsigned int	CONFIG; //Configuration flags
	ADT_Image 	image;

	vector <CS_OpticalInstrument> getList() const;
	const CS_BasicEvent* getEvent(unsigned int n) const;
	CS_OpticalInstrument getItem(unsigned int n) const;
	CS_OpticalInstrument removeItem(unsigned int n);
	unsigned int 	addItem(CS_OpticalInstrument item);
	unsigned int 	getListSize() const;
	unsigned int 	getIndexI() const;
	unsigned int 	getIndexH() const;
	unsigned int 	getIndexL() const;
	long double	getK() const;
	double		getDeep() const;
	long double	getTemp() const;
	long double 	getArea() const;
	long double 	measure(unsigned int type, vector <ADT_Point2D_ui> &points);//, bool drawOutput);
	int 		setIndexI(unsigned int i);
	int 		setIndexL(unsigned int i);
	void		setDeep(double _deep);
	int		undo();
	int 		redo();
	int		saveHistCSV(const char *fileName) const;
	bool 		openCfg(const char *fileName);
	bool 		saveCfg(const char *fileName) const;
	//long double error();	
	void 		calibrate(unsigned int pixels, long double reference);
	//void 		capture(const ADT_Image &i);//consider deleting if image is public
	//void		makeMosaic(vector <string> filesList);
	int 		webService();
	ADT_Celerascope();
	ADT_Celerascope(const char *fileName);
	~ADT_Celerascope();
	
 private:
	vector <CS_BasicEvent*>		historial; //polymorphism
	vector <CS_OpticalInstrument> 	instruments;
	int 		hIndex;
	int 		iIndex;
	double		deep;
	long double 	temporal;
	ADT_Image 	imageTemp;
	void getISO4406Code(CS_ISO4406Event* event);
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#endif // ADT_CELERASCOPE_H
// EOF

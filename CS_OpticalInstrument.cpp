//////////////////////////////////////////////////////////////////////////////
//	ADT_OpticalInstrument.cpp
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	8 Mar 2008 - 16 Jun 2009
//----------------------------------------------------------------------------
//	Clase complementartia de Celerascope
//	Notas:	añadir ordenar por K depues de agregar un lente
//		
//////////////////////////////////////////////////////////////////////////////
#include "CS_OpticalInstrument.h"

#include <iostream>
using namespace std;
//---------------------------------------------------------------------------
CS_Lens::CS_Lens()
{
}
//---------------------------------------------------------------------------
CS_Lens::~CS_Lens()
{
}
//---------------------------------------------------------------------------
CS_Lens::CS_Lens(int zoom_, long double k_)
{
	zoom = zoom_;
	k = k_;
}
//---------------------------------------------------------------------------
unsigned int CS_Lens::getZoom()
{
	return zoom;
}
//---------------------------------------------------------------------------
long double CS_Lens::getK()
{
	return k;
}
//---------------------------------------------------------------------------
void CS_Lens::setK(long double k_)
{
	k = k_;
}
//---------------------------------------------------------------------------
//CS_OpticalInstrument
//---------------------------------------------------------------------------
CS_OpticalInstrument::CS_OpticalInstrument()
{
	index = -1;
	name = "noName";
}
//---------------------------------------------------------------------------
CS_OpticalInstrument::~CS_OpticalInstrument()
{
	lensesList.clear();
	//delete name;
}
//---------------------------------------------------------------------------
CS_OpticalInstrument::CS_OpticalInstrument(const char* name_)
{
	index = -1;
	name = name_; 
}
//---------------------------------------------------------------------------
const char* CS_OpticalInstrument::getName() const
{
	return name.c_str();
}
//---------------------------------------------------------------------------
int CS_OpticalInstrument::getIndex() const
{
	return index;
}
//---------------------------------------------------------------------------
int CS_OpticalInstrument::setIndex(unsigned int i)
{
	if (i < lensesList.size() && i>=0)
		index = i;
 return index;
}
//---------------------------------------------------------------------------
unsigned int CS_OpticalInstrument::getSize() const
{
	return lensesList.size();
}
//---------------------------------------------------------------------------
CS_Lens	CS_OpticalInstrument::getItem(unsigned int n) const
{
	return lensesList[n];
}
//---------------------------------------------------------------------------
unsigned int CS_OpticalInstrument::addItem(CS_Lens item)
{
	lensesList.push_back(item);
	index = lensesList.size()-1;
	// ADD SORT BY K
 return lensesList.size();
}
//---------------------------------------------------------------------------
CS_Lens CS_OpticalInstrument::removeItem(unsigned int n)
{
	CS_Lens lens = lensesList[n];
	lensesList.erase(lensesList.begin()+n);
	index = lensesList.size()-1;
 return lens;
}
//---------------------------------------------------------------------------


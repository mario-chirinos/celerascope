\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{2}{section.1}
\contentsline {section}{\numberline {2}Requisitos}{2}{section.2}
\contentsline {section}{\numberline {3}Configuraci\IeC {\'o}n}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Instrumentos \IeC {\'o}pticos}{2}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Agregar}{2}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Eliminar}{2}{subsubsection.3.1.2}
\contentsline {subsection}{\numberline {3.2}Sistema de unidades}{2}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Opciones de conteo}{3}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Dispositivos de v\IeC {\'\i }deo}{3}{subsection.3.4}
\contentsline {section}{\numberline {4}Im\IeC {\'a}genes}{3}{section.4}
\contentsline {subsection}{\numberline {4.1}Capturar im\IeC {\'a}genes}{3}{subsection.4.1}
\contentsline {section}{\numberline {5}Medici\IeC {\'o}n}{3}{section.5}
\contentsline {subsection}{\numberline {5.1}Selecci\IeC {\'o}n de instrumento \IeC {\'o}ptico}{4}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Calibraci\IeC {\'o}n}{4}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Longitud}{4}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Per\IeC {\'\i }metro}{4}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}\IeC {\'A}rea}{5}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}\IeC {\'A}ngulo}{5}{subsection.5.6}
\contentsline {subsection}{\numberline {5.7}Guardar resultados}{5}{subsection.5.7}
\contentsline {section}{\numberline {6}Conteo de part\IeC {\'\i }culas}{5}{section.6}
\contentsline {subsection}{\numberline {6.1}Requisitos}{6}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Conteo}{6}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Confiabilidad}{6}{subsection.6.3}
\contentsline {section}{\numberline {7}Captura peri\IeC {\'o}dica de im\IeC {\'a}genes}{6}{section.7}
\contentsline {section}{\numberline {8}Soporte}{6}{section.8}

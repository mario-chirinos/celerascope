#include <iostream>
#include <sstream>
#include "ADTlib.h"
//#include "CSlib.h"
#include "ADT_Celerascope.h"

using namespace std;

string intToStr(int i)
{
	stringstream out;
	out << i;
 return out.str();
}
//-----------------------------------------------------------
int main(int argc, char *argv[])
{	
	cout << "Celerascope V2.0"<< endl;
	/*if(argc < 0)
	{
		cerr << "too few arguments" << endl;
		return EXIT_FAILURE;
	}*/




ADT_Image imagenV("IMG_2213.JPG");
imagenV.writeToFile("imV.jpg", "jpg");

ADT_Image imagen2V;
imagen2V=(imagenV);
imagen2V.writeToFile("im2V.jpg", "jpg");
imagenV = ADT_Image("IMG_2213.JPG");



ADT_Celerascope celerascope;
cout << "CONFIG: " << celerascope.CONFIG << endl;
ADT_Image imagen3V;
	celerascope.image = imagenV;
	//	celerascope = imagen3V;
	imagen3V.writeToFile("im3V.jpg", "jpg");
	for(int j=1; j<=3; j++)
	{	
		
		string s = "Micro"+intToStr(j);
		CS_OpticalInstrument instrument(s.c_str());
		for(int i=1; i<=5; i++)
		{
			CS_Lens lente(i*10+j, i*10+i*0.1);
			instrument.addItem(lente);
		}
		instrument.removeItem(3);
		celerascope.addItem(instrument);
	}

	celerascope.removeItem(1);
	celerascope.calibrate(350, (double)50/1000000);
	for(unsigned int j=0; j<celerascope.getListSize(); j++)
	{	
		CS_OpticalInstrument instrument = celerascope.getItem(j);
		cout << instrument.getName()<< ": " << endl;
		cout << "   " << instrument.getSize()<< " items" << endl;
		for(unsigned int i=0; i<instrument.getSize(); i++)
		{
			CS_Lens lente = instrument.getItem(i);
			cout << "      " << lente.getZoom()<< "X" << " - " << lente.getK() << endl;
		}
	}	
	cout << endl << "copia" << endl;
	celerascope.setDeep(0.123456);
	celerascope.saveCfg("CS.cfg");
	ADT_Celerascope celerascope2("CS.cfg");
	cout << "CONFIG: " << celerascope2.CONFIG << endl;

	for(unsigned int j=0; j<celerascope2.getListSize(); j++)
	{	
		CS_OpticalInstrument instrument = celerascope2.getItem(j);
		cout << instrument.getName()<< ": " << endl;
		cout << "   " << instrument.getSize()<< " items" << endl;
		for(unsigned int i=0; i<instrument.getSize(); i++)
		{
			CS_Lens lente = instrument.getItem(i);
			cout << "      " << lente.getZoom()<< "X" << " - " << lente.getK() << endl;
		}
	}
	cout << "Deep: " << celerascope2.getDeep() << " m" << endl;
	cout << endl << "medidas" << endl;
	vector <ADT_Point2D_ui> v;
	v.push_back(ADT_Point2D_ui(1,1));	
	v.push_back(ADT_Point2D_ui(2,1));


	double m = celerascope.measure(SLENGTH, v);
	cout << v.size() << " SLENGTH: " << m << " metros, " << celerascope.getTemp() << " pixeles" << endl;
	m = celerascope.measure(ANGLE, v);
	cout << v.size() << " ANGLE: " << m << " grados" << endl;

	v.push_back(ADT_Point2D_ui(1,2));
	m = celerascope.measure(SLENGTH, v);
	cout << v.size() << " SLENGTH: " << m << " metros, " <<  celerascope.getTemp() << " pixeles" << endl;
	m = celerascope.measure(ALENGTH, v);
	cout << v.size() << " ALENGTH: " << m << " metros, " << celerascope.getTemp() << " pixeles" << endl;
	m = celerascope.measure(ANGLE, v);
	cout << v.size() <<" ANGLE: " << m << " grados, " << celerascope.getTemp() << " radianes" << endl;
	
	v.clear();	
	v.push_back(ADT_Point2D_ui(0,0));
	v.push_back(ADT_Point2D_ui(1,0));
	v.push_back(ADT_Point2D_ui(1,1));
	
	m = celerascope.measure(AREA, v);
	cout << v.size() <<" AREA: " << m << " m^2, " << celerascope.getTemp() << " pixeles^2" << endl;

	v.clear();	
	v.push_back(ADT_Point2D_ui(0,0));
	v.push_back(ADT_Point2D_ui(1,0));
	v.push_back(ADT_Point2D_ui(1,1));
	v.push_back(ADT_Point2D_ui(0,1));

	m = celerascope.measure(AREA, v);
	cout << v.size() <<" AREA: " << m << " m^2, " << celerascope.getTemp() << " pixeles^2" << endl;

	v.clear();	
	v.push_back(ADT_Point2D_ui(0,0));
	v.push_back(ADT_Point2D_ui(2,0));
	v.push_back(ADT_Point2D_ui(2,2));
	v.push_back(ADT_Point2D_ui(1,2));
	v.push_back(ADT_Point2D_ui(1,1));
	v.push_back(ADT_Point2D_ui(0,1));

	m = celerascope.measure(AREA, v);
	cout << v.size() <<" AREA: " << m << " m^2, " << celerascope.getTemp() << " pixeles^2" << endl;
	
	celerascope.image =ADT_Image("LevaduraScerevisea04X.jpg");
	v.clear();
	m = celerascope.measure(PPV, v);
	cout << "PPV: " << m << " p/m^3, " << celerascope.getTemp() << " particulas" << endl;
	celerascope.saveHistCSV("hist.csv");

	v.clear();
	m = celerascope.measure(PPV, v);
	cout << "PPV: " << m << " p/m^3, " << celerascope.getTemp() << " particulas" << endl;
	celerascope.saveHistCSV("hist.csv");
	v.clear();

	m = celerascope.measure(PPV, v);
	cout << "PPV: " << m << " p/m^3, " << celerascope.getTemp() << " particulas" << endl;
	celerascope.saveHistCSV("hist.csv");

return 0;
}

////////////////////////////////////////////////////////////////////////////////
//	ADT_Celerascope.cpp
//	Mario Chirinos Colunga
//	Aurea Desarrollo Tecnológico.
//	7 Mar 2008 - 21 may 11
//------------------------------------------------------------------------------
//	Clase base de Celerascope
//	Notas:	las unidades de referencia deben de estar en metros
//		añadir ordenar por nombre depues de agregar un instrumento
//	V 2.1 2012-02-07
////////////////////////////////////////////////////////////////////////////////
#include "ADT_Celerascope.h"
#include "CS_OpticalInstrument.h"
#include "CS_Functions.h"
#include <iostream>
#include <fstream>
#include <cmath>	
#include <cstring>
#include <cstdlib>
#include <climits>
using namespace std;
//------------------------------------------------------------------------------
ADT_Celerascope::ADT_Celerascope()
{
	//image = new ADT_Image();
	//imageTemp = new ADT_Image();
	hIndex=0;
	deep = 0.0001;//0.1mm
	CONFIG = 0;
	CONFIG = COUNT | UNITS | NOTATION | VOLUME | ANGLE;
}
//------------------------------------------------------------------------------
ADT_Celerascope::~ADT_Celerascope()
{
	//delete image;
	//delete imageTemp;
	hIndex = -1;
	iIndex = -1;
	deep = 0.0001;//0.1mm;
	temporal = 0;
	historial.clear();
	instruments.clear();
}
//------------------------------------------------------------------------------
ADT_Celerascope::ADT_Celerascope(const char *name)
{
	//image = new ADT_Image();
	//imageTemp = new ADT_Image();
	hIndex=-1;
//	deep = 0.0001;//0.1mm
	openCfg(name);
}
//------------------------------------------------------------------------------
/*void ADT_Celerascope::capture(const ADT_Image &i)
{	
	image=i;
	imageTemp=i;
}*/
//------------------------------------------------------------------------------
int ADT_Celerascope::webService()
{

	return false;
}
//------------------------------------------------------------------------------
unsigned int ADT_Celerascope::getListSize() const
{
	return instruments.size();
}
//------------------------------------------------------------------------------
unsigned int ADT_Celerascope::getIndexI() const
{
	return iIndex;
}
//------------------------------------------------------------------------------
unsigned int ADT_Celerascope::getIndexH() const
{
	return hIndex;
}
//------------------------------------------------------------------------------
unsigned int ADT_Celerascope::getIndexL() const
{
	return instruments[iIndex].getIndex();
}
//------------------------------------------------------------------------------
const CS_BasicEvent* ADT_Celerascope::getEvent(unsigned int n) const
{
	if(n >= historial.size())
	{
		cerr << "Event index out of range" << endl;
		exit(1);
	}
	return (CS_BasicEvent*) historial[n];
}
//------------------------------------------------------------------------------
int ADT_Celerascope::setIndexI(unsigned int i)
{
	if (i < instruments.size() && i >=0)
		iIndex = i;
 return iIndex;
}
//------------------------------------------------------------------------------
int ADT_Celerascope::setIndexL(unsigned int i)
{ 
	return instruments[iIndex].setIndex(i);	
}
//------------------------------------------------------------------------------
void ADT_Celerascope::setDeep(double _deep)
{
	deep = _deep;
}
//------------------------------------------------------------------------------
vector <CS_OpticalInstrument> ADT_Celerascope::getList() const
{
	return instruments;
}
//------------------------------------------------------------------------------
CS_OpticalInstrument ADT_Celerascope::getItem(unsigned int n) const
{
	return instruments[n];
}
//------------------------------------------------------------------------------
long double ADT_Celerascope::getK() const
{
	CS_Lens lens = instruments[iIndex].getItem(instruments[iIndex].getIndex());
	return lens.getK();
}
//------------------------------------------------------------------------------
double ADT_Celerascope::getDeep() const
{
	return deep;
}
//------------------------------------------------------------------------------
long double ADT_Celerascope::getTemp() const
{
	return temporal;
}
//------------------------------------------------------------------------------
long double ADT_Celerascope::getArea() const
{
	return image.getWidth()*image.getHeight()*getK()*getK();
}
//------------------------------------------------------------------------------
unsigned int ADT_Celerascope::addItem(CS_OpticalInstrument item)
{
	instruments.push_back(item);
	iIndex = instruments.size()-1;
	//ADD SORT BY NAME
 	return instruments.size();
}
//------------------------------------------------------------------------------
CS_OpticalInstrument ADT_Celerascope::removeItem(unsigned int n)
{
	CS_OpticalInstrument instrument = instruments[n];
	instruments.erase(instruments.begin()+n);
	iIndex = instruments.size()-1;
return instrument;
}
//------------------------------------------------------------------------------
void ADT_Celerascope::calibrate(unsigned int pixels, long double reference) //checar index depues de la calibracion
{
	//cout << "instruments.size(): " << instruments.size() << ", iIndex: " << iIndex << ", instruments[iIndex].getIndex(): " << instruments[iIndex].getIndex() << " instruments[iIndex].getSize(): " << instruments[iIndex].getSize() << endl;
	CS_Lens lens = instruments[iIndex].removeItem(instruments[iIndex].getIndex());
	lens.setK(reference/pixels);
	instruments[iIndex].addItem(lens);
}
//------------------------------------------------------------------------------
long double ADT_Celerascope::measure(unsigned int type, vector <ADT_Point2D_ui> &points)
{
 long double measure;
 CS_BasicEvent *event;
	switch (type)
	{
	  case SLENGTH:	
			event = new CS_BasicEvent;				
			temporal = sLength(points);
			measure = temporal * getK();
		break;
	  case ALENGTH:
			event = new CS_BasicEvent;
			temporal = aLength(points);
			measure = temporal * getK();
		break;
	  case AREA:
			event = new CS_BasicEvent;
			temporal =  polyArea(points);
			measure = temporal * getK() * getK();
		break;
	  case ANGLE:
			event = new CS_BasicEvent;
			temporal = angle(points);
			measure = temporal * (CONFIG&ANGLE? 180/M_PI:1);
		break;
	  case AUTOAREA:
			//event = new CS_BasicEvent;
			measure =  0;
		break;
	  case PPV:	
			event = new CS_CountEvent;				
			temporal = countParticles(image, (CONFIG&COUNT)!=0, ((CS_CountEvent*)event)->segments, ((CS_CountEvent*)event)->median);//prticles areas are stored in event.point
			measure = temporal / (getArea()*deep*((CONFIG&VOLUME)!=0?1:1000));
		break;
	  case ISO4406:	
			event = new CS_ISO4406Event;
			temporal = countParticles(image, (CONFIG&COUNT)!=0, ((CS_CountEvent*)event)->segments, ((CS_CountEvent*)event)->median);//prticles areas are stored in event.point				
			measure = temporal / (getArea()*deep*((CONFIG&VOLUME)!=0?1:1000));
			getISO4406Code((CS_ISO4406Event*)event);
		break;
	  default:
			measure = -1;
			cerr<<"Invalid measurement type"<< endl;
	}
	event->points	= points;
	event->type	= type;
	event->result	= measure;
	event->temp	= temporal;
	if (temporal != 0)
	{	 //cout << "hIndex: " << hIndex << "historial.size: "<< historial.size()<<endl;
		if (hIndex != (int) historial.size()-1 && hIndex>0)
			historial.erase(historial.begin()+hIndex+1, historial.end());		
		
		historial.push_back(event);
		hIndex = historial.size()-1;	/**/	
	}
 return measure;
}
//------------------------------------------------------------------------------

int ADT_Celerascope::undo()
{
	if(historial.size()>0)
		hIndex--;
return	 historial.size()-1;
}
//------------------------------------------------------------------------------
int ADT_Celerascope::redo()
{
	if((int)historial.size()>hIndex+1)
		hIndex++;
return	 historial.size()-1;
}
//------------------------------------------------------------------------------
bool ADT_Celerascope::saveCfg(const char *fileName) const
{

	FILE *file;
	if ((file = fopen(fileName, "wb")) == NULL)
	{
		cerr<< "Cannot create configuration file" << endl;
		return false;
	}

	fwrite(&CONFIG, sizeof(unsigned int), 1, file);

	int nI=instruments.size();	
	 fwrite(&nI, sizeof(int), 1, file);
   	
	char nameI[100];

	for(int i=0; i<nI; i++)
	{
		strcpy(nameI, instruments[i].getName());
		int nameLenght=strlen(nameI);//+1;

		fwrite(&nameLenght, sizeof(int), 1, file);
		 fwrite(&nameI, nameLenght, 1, file);
		int indexL = instruments[i].getIndex();
		   fwrite(&indexL, sizeof(int), 1, file);

		int nLenses=instruments[i].getSize();
		 fwrite(&nLenses, sizeof(int), 1, file);
		
		for(int j=0; j<nLenses; j++)
		{
		        CS_Lens lens = instruments[i].getItem(j);
			int zoomL = lens.getZoom();
		         fwrite(&zoomL, sizeof(int), 1, file);
			double kL = lens.getK();
		         fwrite(&kL, sizeof(double), 1, file);
		}
	}
	fwrite(&iIndex, sizeof(int), 1, file);
	fwrite(&deep, sizeof(double), 1, file);
   	fclose(file);

 return true;
}
//------------------------------------------------------------------------------
bool ADT_Celerascope::openCfg(const char *fileName)
{  
	FILE *file;

	if ((file = fopen(fileName, "rb")) == NULL)
	{
		//creear elemento por defecto;
		cerr<< "Cannot open configuration file: creating default configuration file." << endl;
		CS_OpticalInstrument instrument("celerascope default");
		CS_Lens lente(1, 1);
		instrument.addItem(lente);
		this->addItem(instrument);
		this->setDeep(0.001);
		cout << "saving cfg file" << endl;
		this->saveCfg(fileName);
		return false;
	}
//	fclose(file);
//	file = fopen(fileName, "rb");
	cout << "reading cfg file" << endl;
	fread(&CONFIG, sizeof(unsigned int), 1, file);

	instruments.clear();

	int nI;
	 fread(&nI, sizeof(int), 1, file);
   
	for(int i=0; i<nI; i++)
	{
		char nameI[100]="";
		int nLenght;
		fread(&nLenght, sizeof(int), 1, file);
		fread(&nameI, nLenght, 1, file);
	
		CS_OpticalInstrument instrument(string(nameI).c_str());
		int indexLens;
		 fread(&indexLens, sizeof(int), 1, file);

		int nLenses;
		fread(&nLenses, sizeof(int), 1, file);

		for(int j=0; j<nLenses; j++)
		{
			int zoomLens;
			 fread(&zoomLens, sizeof(int), 1, file);
			double kLens;
			 fread(&kLens, sizeof(double), 1, file);

                	CS_Lens lens(zoomLens, kLens);
                	instrument.addItem(lens);
		}	
		instrument.setIndex(indexLens);
		addItem(instrument);
	}
	fread(&iIndex, sizeof(int), 1, file);
	fread(&deep, sizeof(double), 1, file);
      fclose(file);
 return true;
}
//------------------------------------------------------------------------------
int ADT_Celerascope::saveHistCSV(const char *fileName) const
{

  ofstream csvfile;
  csvfile.open (fileName);
	csvfile << "Tipo, Valor, Unidad" << endl;
	for(unsigned int i=0; i<historial.size(); i++)
	{
		csvfile << historial[i]->type <<", "<< historial[i]->result << ", " << temporal << endl;
	}

  csvfile.close();
  return 0;
}
//------------------------------------------------------------------------------
void ADT_Celerascope::getISO4406Code(CS_ISO4406Event* event)
{
// ISO4406 CODE
float ISO4406Code[30] = {0.0, 0.1, 0.2, 0.4, 0.8, 0.16, 0.32, 0.64, 1.3, 2.5, 5,
			10, 20, 40, 80, 160, 320, 640, 1300, 2500, 5000,
			10000, 20000, 40000, 80000, 160000, 320000, 640000, 1300000, 2500000};

	event->lengths.clear();
	event->ppv[0] = event->ppv[1] = event->ppv[2] = 0;
	event->code[0] = event->code[1] = event->code[2] = 0;
	for (unsigned int i=0; i<event->segments.size(); i++)
	{
		unsigned int minX = INT_MAX , minY = INT_MAX ;
		unsigned int maxX=0, maxY=0;
		for(unsigned int j=0; j<event->segments[i].size(); j++)
		{
			if(event->segments[i][j].x<minX)
				minX=event->segments[i][j].x;
			if(event->segments[i][j].y<minY)
				minY=event->segments[i][j].y;
			if(event->segments[i][j].x>maxX)
				maxX=event->segments[i][j].x;
			if(event->segments[i][j].y>minY)
				maxY=event->segments[i][j].y;
		}
		
		float particleLength = sqrt((maxX-minX)*(maxX-minX)+(maxY-minY)*(maxY-minY));
		event->lengths.push_back(particleLength*getK());
	}

	for(int i=0; i<event->lengths.size(); i++)
	{
		if(event->lengths[i]>0.000004)
			event->ppv[0]++;
		if(event->lengths[i]>0.000006)
			event->ppv[1]++;
		if(event->lengths[i]>0.000014)
			event->ppv[2]++;
	}
	for(int i=0; i<3; i++)
	{
		event->ppv[i]/=(getArea()*deep*1000000); //Particles per miliLiter
	}

	for(int i=0; i<3; i++)
		for(int n=0; n<30; n++)
		{
			if(event->ppv[i]<=ISO4406Code[n])
				break;
			event->code[i]=n;
		}
}
//---------------------------------------------------------------------------
